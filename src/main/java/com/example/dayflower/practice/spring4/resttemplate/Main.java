package com.example.dayflower.practice.spring4.resttemplate;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Slf4j
public class Main {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		WikipediaService wikipediaService = context.getBean(WikipediaService.class);
		log.info("lookuped: {}", wikipediaService.getPage("かまぼこ"));
	}

}
