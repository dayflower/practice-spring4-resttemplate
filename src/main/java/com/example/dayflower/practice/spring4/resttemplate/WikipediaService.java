package com.example.dayflower.practice.spring4.resttemplate;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Component
public class WikipediaService {
	@Autowired
	private RestTemplate restTemplate;

	public PageModel getPage(String title) {
		UriComponents uri = UriComponentsBuilder.fromUriString("http://ja.wikipedia.org/w/api.php")
			.queryParam("action", "query")
			.queryParam("prop", "revisions")
			.queryParam("rvprop", "content")
			.query("redirects")
			.query("continue")
			.queryParam("format", "json")
			.queryParam("titles", title)
			.build();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<>(headers);

//		ResponseEntity<QueryPageResult> res = restTemplate.getForEntity(uri.toString(), QueryPageResult.class);
		ResponseEntity<QueryPageResult> res = restTemplate.exchange(uri.toString(), HttpMethod.GET, entity, QueryPageResult.class);
		if (! res.getStatusCode().is2xxSuccessful()) {
			log.error("failed: {}", res.getStatusCode().toString());
		}

		return res.getBody().getFirstPage();
	}

	@Data
	@Builder
	public static class PageModel {

		private boolean found;
		private int pageId;
		private String title;
		private String content;
	}

	@Data
	public static class QueryPageResult {

		private QueryContent query;

		public PageModel getFirstPage() {
			PageModel.PageModelBuilder builder = PageModel.builder().found(false);

			Map.Entry<Integer, PageContent> entry = this.getQuery().getPages().entrySet().iterator().next();
			if (entry != null) {
				PageContent content = entry.getValue();
				if (content != null) {
					builder = builder.title(content.getTitle()).pageId(content.getPageId());
					List<RevisionContent> revs = content.getRevisions();
					if (revs != null && revs.size() > 0) {
						RevisionContent rev = content.getRevisions().get(0);
						if (rev != null) {
							if (rev.getMissing() != null) {
								return builder.found(false).build();
							}

							return builder.found(true).content(rev.getContent()).build();
						}
					}
				}
			}

			return builder.found(false).build();
		}
	}

	@Data
	public static class QueryContent {

		private Map<Integer, PageContent> pages;
	}

	@Data
	public static class PageContent {

		@JsonProperty("pageid")
		private int pageId;
		private int ns;
		private String title;
		private List<RevisionContent> revisions;
	}

	@Data
	public static class RevisionContent {

		@JsonProperty("contentformat")
		private String contentFormat;
		@JsonProperty("contentmodel")
		private String contentModel;
		@JsonProperty("*")
		private String content;
		private String missing;
	}
}
