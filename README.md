## 目的

* HTTP client として Spring の RestTemplate を使う。

## 苦労したところ

* 最初クエリパラメータを getForEntity() 等の引数に食わせていて、うまくいかないなと悩んでいたが、 getForEntity() 等の引数で受けるのは URL の template 引数展開部分だった。固定されたクエリパラメータであればテンプレート展開でよいが、柔軟なクエリパラメータの設定をおこないたい場合、 UriComponentsBuilder を使うのがいいっぽい。
    * しかし戻り値が UriComponent になってしまい、 Uri が欲しいところにどう渡せばよいのかわからなかった。今は toString() して String として渡しているけど。
